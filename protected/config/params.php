<?php

// this contains the application parameters that can be maintained via GUI
return array(
    'appTitle'=>'Yii Blog Demo with bootstrap',
    'appDescription'=>'Bringing together the Yii Blog and Bootstrap',
    'supported_locales'=>array('en','de'),
    // this is displayed in the header section
    'title'=>'My Yii Blog',
    // this is used in error pages
    'adminEmail'=>'webmaster@example.com',
    // number of posts displayed per page
    'postsPerPage'=>10,
    // maximum number of items that can be displayed in monthly archives portlet
    'monthlyArchivesCount'=>10,
    // maximum number of posts that can be displayed in recent posts portlet
    'recentPostCount'=>10,
    // maximum number of comments that can be displayed in recent comments portlet
    'recentCommentCount'=>10,
    // maximum number of tags that can be displayed in tag cloud portlet
    'tagCloudCount'=>20,
    // whether post comments need to be approved before published
    'commentNeedApproval'=>true,
    // the copyright information displayed in the footer section
    'copyrightInfo'=>'Copyright &copy; XXXX by My Company.',
);
